
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> wordText = new HashMap<>();
        try {
            Scanner scanner = new Scanner(new FileInputStream("resource\\text.txt"));
            while (scanner.hasNextLine()) {
                Scanner scannerLine = new Scanner(scanner.nextLine());
                while (scannerLine.hasNext()) {
                    String word = scannerLine.next();
                    if (wordText.containsKey(word)) wordText.put(word, wordText.get(word) + 1);
                    else wordText.put(word, 1);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        printValues(wordText);
    }
    public static void printValues(Map<String, Integer> wordText)
    {
        for(Map.Entry<String, Integer> basket : wordText.entrySet())
        {
            Integer value = basket.getValue();
            String key = basket.getKey();
            System.out.println(key+" - "+value);
        }
    }
}


